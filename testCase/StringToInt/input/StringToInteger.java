/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nakamol.midterm;

import java.util.Scanner;

/**
 *
 * @author OS
 */
public class StringToInteger {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        System.out.print("input String: ");
        String s = kb.nextLine();

        System.out.print("Result is: ");
        System.out.println(String.valueOf(s));
  
    }
    
}
