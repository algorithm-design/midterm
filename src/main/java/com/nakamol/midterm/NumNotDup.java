/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.nakamol.midterm;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author OS
 */
public class NumNotDup {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        ArrayList<Integer> A = new ArrayList<Integer>();

        System.out.print("input number: ");
        int n = kb.nextInt();
        for (int i = 0; i < n; i++) {
            int num = kb.nextInt();
            A.add(num);
        }

        findNum(A);

    }

    public static void findNum(ArrayList<Integer> A) {
        int xx = 0;
        //ArrayList<Integer> xx = new ArrayList<Integer>();
        ArrayList<Integer> x = new ArrayList<Integer>();
        for (Integer list : A) {

            if (!x.contains(list)) {
                x.add(list);
            } else {
                xx = list;
                x.remove(list);
            }
        }
        System.out.println("Output is: " + x);
    }
}
